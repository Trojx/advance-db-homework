package me.trojx.advancedbhomework.controller;

import me.trojx.advancedbhomework.entity.Car;
import me.trojx.advancedbhomework.entity.CurrentLocation;
import me.trojx.advancedbhomework.entity.Location;
import me.trojx.advancedbhomework.repository.CurrentLocationRepository;
import me.trojx.advancedbhomework.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by Trojx(饶建勋) on 2019/5/19.
 * E-mail: raojianxun@126.com
 * Tel: 18817875047
 */
@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    CurrentLocationRepository currentLocationRepository;

    @GetMapping("/history")
    public List<Location> findLocationInTime(@RequestParam Long carId, @RequestParam Long startTime, @RequestParam Long endTime){
        Car car=new Car();
        car.setId(carId);
        return locationRepository.findLocationByCarEqualsAndTimeBetweenOrderByTimeAsc(car,new Date(startTime),new Date(endTime));
    }

    @GetMapping("/current")
    public Location findCurrentLocation(@RequestParam Long carId){
        Car car=new Car();
        car.setId(carId);
        return locationRepository.findFirstByCarEqualsOrderByTimeDesc(car);
    }

    @PostMapping
    public Location addLocation(@RequestParam Long carId,@RequestParam Float lat,@RequestParam Float lon){
        Car car=new Car(){{setId(carId);}};
        Date now=new Date();
        Location location=new Location();
        location.setCar(car);
        location.setLat(lat);
        location.setLon(lon);
        location.setTime(now);

        //保存当前位置
        CurrentLocation currentLocation=currentLocationRepository.findFirstByCarEquals(car);
        if (currentLocation==null){
            currentLocation=new CurrentLocation();
            currentLocation.setCar(car);
        }
        currentLocation.setLat(lat);
        currentLocation.setLon(lon);
        currentLocation.setTime(now);
        currentLocationRepository.save(currentLocation);

        return locationRepository.save(location);
    }
}
