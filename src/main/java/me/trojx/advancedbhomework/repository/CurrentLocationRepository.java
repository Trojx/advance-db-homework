package me.trojx.advancedbhomework.repository;

import me.trojx.advancedbhomework.entity.Car;
import me.trojx.advancedbhomework.entity.CurrentLocation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Trojx(饶建勋) on 2019/5/19.
 * E-mail: raojianxun@126.com
 * Tel: 18817875047
 */
public interface CurrentLocationRepository extends JpaRepository<CurrentLocation,Long> {
    CurrentLocation findFirstByCarEquals(Car car);

}
