package me.trojx.advancedbhomework.controller;

import me.trojx.advancedbhomework.entity.Car;
import me.trojx.advancedbhomework.entity.CurrentLocation;
import me.trojx.advancedbhomework.repository.CurrentLocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Trojx(饶建勋) on 2019/5/19.
 * E-mail: raojianxun@126.com
 * Tel: 18817875047
 */
@RestController
@RequestMapping("/currentLocation")
public class CurrentLocationController {

    @Autowired
    CurrentLocationRepository currentLocationRepository;

    @GetMapping()
    public CurrentLocation findCurrentLocation(@RequestParam Long carId){
        return currentLocationRepository.findFirstByCarEquals(new Car(){{setId(carId);}});
    }
}
