package me.trojx.advancedbhomework.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Trojx(饶建勋) on 2019/5/19.
 * E-mail: raojianxun@126.com
 * Tel: 18817875047
 */
@Entity
@Table(name="car")
@Data
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" })
public class Car implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "license")
    private String license;

    @Column(name = "owner")
    private String owner;
}
