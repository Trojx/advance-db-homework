package me.trojx.advancedbhomework.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Trojx(饶建勋) on 2019/5/19.
 * E-mail: raojianxun@126.com
 * Tel: 18817875047
 */
@Entity
@Table(name = "location")
@Data
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" })
public class Location implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn(name = "car_id")
    private Car car;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "lon")
    private Float lon;

    @Column(name = "time")
    private Date time;
}
