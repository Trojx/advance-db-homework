package me.trojx.advancedbhomework.repository;

import me.trojx.advancedbhomework.entity.Car;
import me.trojx.advancedbhomework.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Trojx(饶建勋) on 2019/5/19.
 * E-mail: raojianxun@126.com
 * Tel: 18817875047
 */
public interface LocationRepository extends JpaRepository<Location,Long> {
    List<Location> findLocationByCarEqualsAndTimeBetweenOrderByTimeAsc(Car car, Date startTime, Date endTime);
    Location findFirstByCarEqualsOrderByTimeDesc(Car car);
}
