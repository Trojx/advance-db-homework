package me.trojx.advancedbhomework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvanceDbHomeworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdvanceDbHomeworkApplication.class, args);
    }

}
