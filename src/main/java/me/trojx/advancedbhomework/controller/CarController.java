package me.trojx.advancedbhomework.controller;

import me.trojx.advancedbhomework.entity.Car;
import me.trojx.advancedbhomework.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Trojx(饶建勋) on 2019/5/19.
 * E-mail: raojianxun@126.com
 * Tel: 18817875047
 */
@RestController
@RequestMapping("/car")
public class CarController {

    @Autowired
    CarRepository carRepository;

    @GetMapping("/{id}")
    public Car getCarById(@PathVariable Long id){
        return carRepository.getOne(id);
    }

    @PostMapping
    public Car addCar(@RequestBody Car car){
        return carRepository.save(car);
    }

    @GetMapping
    public List<Car> getCarList(){
        return carRepository.findAll();
    }
}
